import-module smlets

$itemsWithAssets = @()
$itemsWithoutAssets = @()
$outputFilePath = "C:\temp\"
$outputFileName_WithoutAssets = "computersWithoutAssets.csv"
$outputFileName_WithAssets = "computersWithAssets.csv"
$saveAsXLSX = $false

$relatedCiresonAssetDisplayName = @()
$relatedCiresonAssetVersion = @()
$relatedCiresonAssetSoftwarePattern = @()

Function fn-output-as-xml
{
    Param(
        $inputCSV
        )

    $newXLSX = $inputCSV.Split(".")[0]

    $xl = new-object -comobject excel.application
    $xl.visible = $false
    $Workbook = $xl.workbooks.open($inputCSV)
    $Worksheets = $Workbooks.worksheets

    $Workbook.SaveAs($newXLSX,51)
    $Workbook.Saved = $True

    $xl.Quit()

    $inputCSV = $null
    $xl =  $null
}


$allWindowsComputers = get-scsmobject -class (Get-SCsmclass -name "Microsoft.Windows.Computer$")

$relatedHardwareAssetRelationship_obj = Get-SCSMRelationshipClass -name 'Cireson.AssetManagement.HardwareAssetHasAssociatedCI'

#Just for reference - how to see what objects are related to an object
#Get-SCSMRelationshipObject -ByTarget <software item object that we *know* is associated so we can see the relationship>

foreach ($windowsComputer in $allWindowsComputers)
{
    $relatedCiresonAsset = Get-SCSMRelationshipObject -targetobject $windowsComputer -TargetRelationship $relatedHardwareAssetRelationship_obj

    if ($relatedCiresonAsset.count -ge 1)
    {
        foreach ($asset in $relatedCiresonAsset)
        {
                    $relatedCiresonAssetDisplayName += $relatedCiresonAsset.SourceObject.Displayname
        }

        $relatedCiresonAssetDisplayName = ($relatedCiresonAssetDisplayName | select -Unique) -join ", "

        Add-Member -InputObject $windowsComputer -MemberType NoteProperty -Name "RelatedCiresonHardwareAssetDisplayName" -Value $relatedCiresonAssetDisplayName -Force

        $itemsWithAssets += $windowsComputer
    }
    elseif ($relatedCiresonAsset.count -eq 0)
    {
        $itemsWithoutAssets += $windowsComputer
    }

    $relatedCiresonAssetDisplayName = @()

}

if (!$outputFilePath)
{
    mkdir $outputFilePath
}

$itemsWithoutAssets | select displayname  | Export-Csv -Path ($outputFilePath + $outputFileName_WithoutAssets) -Encoding ascii -NoTypeInformation
$itemsWithAssets | select displayname, RelatedCiresonSoftwareAssetDisplayName  | Export-Csv -Path ($outputFilePath + $outputFileName_WithAssets) -Encoding ascii -NoTypeInformation

remove-module smlets

if ($saveAsXLSX)
{
    fn-output-as-xml -inputCSV ($outputFilePath + $outputFileName_WithoutAssets)
    fn-output-as-xml -inputCSV ($outputFilePath + $outputFileName_WithAssets)
}