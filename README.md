cireson-hardware-asset-reconcile
===================

Allows you to reconcile windows computers and hardware assets in Microsoft SCSM

**Summary:**

- This script runs with no parameters!
- It will find any windows computer in SCSM and then create two arrays
	- One array will have any windows computer that is already associated with a Cireson Hardware Asset
	- The other array will have any windows computer that is *not* already associated with a Cireson Hardware Asset

**Changelog:**


- Version .01 (Sept 10, 2014)
	- Initial Version
	- Minimal error checking, but it works!